/**
 * @autor Antonio
 * @version 1.0
 * @since 20/06/2020
 */
package antoniodavid.practicarecuperacioned;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
 
    /**
    * Clase principal del programa
    */
public class Main {
   
    
    private static List<Casa> casas;
    private static List<Confinado> confinados;
    private static Scanner sc;
    
    public static void main(String[] args) {
         casas = new ArrayList<Casa>();
         confinados = new ArrayList<Confinado>();
        
        
        Scanner sc = new Scanner(System.in);
         int opcion = -1;
        do{
            System.out.println("== MENU PRINCIPAL ==");

            System.out.println("1) Añadir Casa");
            System.out.println("2) Eliminar Casa");
            System.out.println("3) Añadir Confinado");
            System.out.println("4) Eliminar Confinado");
            System.out.println("5) Asignar confinado a casa");       
            
            System.out.println("6) Mostrar casas");
            System.out.println("7) Mostrar confinados");
            
            System.out.println("0) Salir");

            System.out.println("Introduce la opcion deseada: ");
            opcion = sc.nextInt();


            switch(opcion){
                case 1:
                    AnadirCasa();
                    break;

                case 2:
                    EliminarCasa();
                    break;

                case 3:
                    AnadirConfinado();
                    break;

                case 4:
                    EliminarConfinado();
                    break;

                case 5:
                    AsignarCasaConfinado();
                    break;
                
                case 0:
                    System.out.println("Saliendo...");
                    break;

                default:
                    System.out.println("Opcion invalida, pruebe de nuevo");
            }
        
        }while(opcion!=0);
    }
    
     
    /**
     * Añade una casa nueva, pidiendo al usuario el ID y si tiene jardin o no.
     * Sigue preguntando hasta que se introducen valores válidos.
     */
         public static void AnadirCasa() {
                Casa c = new Casa();
                int id = -1;
                String respuesta = "";
                System.out.println("== AÑADIR CASA ==");

                do{
                    System.out.println("Introduce el id: ");
                    try{
                        id = sc.nextInt();
                        System.out.flush();
                        c.setId(id);
                    }catch(Exception ex){
                        id = -1;
                    }
                }while(id < 0);

                do{
                    System.out.println("Tiene jardin? (S/N): ");
                    respuesta = sc.next().toUpperCase();

                    if(respuesta.equals("S")){
                        c.setTiene_jardin(true);                
                    }else if (respuesta.equals("N")){
                        c.setTiene_jardin(false);                
                    }else {
                        System.out.println("Opcion incorrecta, pruebe de nuevo");
                    }
                }while(!respuesta.equals("S") && !respuesta.equals("N"));

                casas.add(c);      

        /**
        * Añade un confinado nuevo, pidiendo al usuario el ID, nombre y casa en la que está.
        * Sigue preguntando hasta que se introducen valores válidos.
        */
         public static void AnadirConfinado() {
                Confinado conf = new Confinado();
                String nombre = "";
                int casa = -1;
                int id = -1;

                System.out.println("== AÑADIR CONFINADO ==");

                do{
                    System.out.println("Introduce el id: ");
                    try{
                        id = sc.nextInt();
                        System.out.flush();
                        conf.setId(id);
                    }catch(Exception ex){
                        id = -1;
                    }
                }while(id < 0);

                do{
                    System.out.println("Introduce el Nombre: ");
                    nombre = sc.next();

                    if(!nombre.equals("")){
                        conf.setNombre(nombre);                       
                    }else {
                        System.out.println("Introduce un nombre por favor");
                    }
                }while(nombre.equals(""));

                do{
                    System.out.println("Introduce la casa : ");
                    try{
                        casa = sc.nextInt();
                        System.out.flush();
                        conf.setCasa(casa);
                    }catch(Exception ex){
                        casa = -1;
                    }
                }while(casa < 0);

                confinados.add(conf);  
    }
        /**
        * Pide un ID e intenta eliminar la casa si no hay confinados.
        */
    public static void EliminarCasa() {
        int id = -1;
        boolean sePuedeEliminar = true;
        
        System.out.println("== ELIMINAR CASA ==");

        do{
            System.out.println("Introduce el id: ");
            try{
                id = sc.nextInt();
                System.out.flush();                
            }catch(Exception ex){
                id = -1;
            }
        }while(id < 0);
        
        if(casas.size() == 0){
            System.out.println("No hay casas");
        }else{            
            for (int j = 0; j < confinados.size(); j++) {
                Confinado conf = confinados.get(j);
                if(conf.getCasa() == id){
                    sePuedeEliminar = false;
                    System.out.println("En esa casa hay confinados, no se puede eliminar");
                    break;
                }
            }                                
            
            for (int i = 0; i < casas.size(); i++) {
                Casa c = casas.get(i);
                if(c.getId() == id){
                    if(sePuedeEliminar){
                        System.out.println("Casa eliminada");
                        casas.remove(c);
                    }
                }
            }                       
        }
    }
    /**
    * Pide un ID de confinado y lo elimina si lo encuentra.
    */
    public static void EliminarConfinado() {
        int id = -1;        
        boolean eliminado = false;
        
        System.out.println("== ELIMINAR CONFINADO ==");

        do{
            System.out.println("Introduce el id: ");
            try{
                id = sc.nextInt();
                System.out.flush();                
            }catch(Exception ex){
                id = -1;
            }
        }while(id < 0);
        
        if(confinados.size() == 0){
            System.out.println("No hay confinados");
        }else{
            for (Confinado conf : confinados) {
                if(conf.getId() == id){
                    confinados.remove(conf);
                    eliminado = true;
                }
            }
            
            if(eliminado){
                System.out.println("Confinado " + id + " eliminado");
            }else{
                System.out.println("No se ha encontrado al confinado con ID " + id);
            }
        }
    }
    
    /**
     * Pide un ID de confinado y de casa y se lo asigna al confinado si lo encuentra.
     */
    public static void AsignarCasaConfinado() {
        int id = -1;
        int casa = -1;
        boolean cambiado = false;
        
        System.out.println("== ASIGNAR CASA A CONFINADO ==");
        
        do{
            System.out.println("Introduce el id del confinado: ");
            try{
                id = sc.nextInt();
                System.out.flush();                
            }catch(Exception ex){
                id = -1;
            }
        }while(id < 0);
        
        do{
            System.out.println("Introduce el id de la casa: ");
            try{
                casa = sc.nextInt();
                System.out.flush();                
            }catch(Exception ex){
                casa = -1;
            }
        }while(casa < 0);
        
        if(confinados.size() == 0){
            System.out.println("No hay confinados");
        }else{
            
            for(Confinado conf : confinados){
                if(conf.getId() == id){
                    conf.setCasa(casa);
                    cambiado = true;

                }
            }

            if(cambiado){
                System.out.println("Confinado cambiado de casa");
            }
            else{
                System.out.println("No se encontró al confinado");
            }
        
        }
    }
        
    


    
    
}
