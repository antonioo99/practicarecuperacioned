package antoniodavid.practicarecuperacioned;

/**
 *
 * @author Antonio
 * @version 1.0
 */
public class Confinado {
    private int id;
    private String nombre;
    private int casa;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the casa
     */
    public int getCasa() {
        return casa;
    }

    /**
     * @param casa the casa to set
     */
    public void setCasa(int casa) {
        this.casa = casa;
    }
    
}
